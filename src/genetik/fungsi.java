package genetik;

import java.util.Objects;
import java.util.Random;
import javax.swing.table.DefaultTableModel;


public class fungsi {
    
    frm_tsp frame;
    
    public fungsi(frm_tsp frame) {
        this.frame = frame;
    }

    Random random = new Random();
    
    String buat_kromosom(){
        String str,kromosom;
        Integer i,panjang;
        
        panjang=0;
        str = "";
        kromosom = "";
        
        while (panjang < 5){
            i = random.nextInt(5);
            switch(i)
            {
                case 0: str = "B"; break;
                case 1: str = "C"; break;
                case 2: str = "D"; break;
                case 3: str = "E"; break;
                case 4: str = "F"; break;
            }
            int cek = kromosom.indexOf(str);
            if(cek<0){
                kromosom = kromosom + str;
            }
            panjang = kromosom.length();
        }
        return kromosom;    
    }
    
    Integer hitung_jarak(String kromosom){
        Integer jarak,pjg,i,jrk;
        String k,kt;
        
        jarak = 0;
        kt = "";
        i=0;
        k = "A"+kromosom+"A";
        pjg = k.length();
        
        while(i<pjg-1){
            kt = k.substring(i, i+2);
            jrk = tentukan_jarak(kt);
            jarak = jarak + jrk;
            i++;
        }
        
        return jarak;
    }
    
    public void isi_random() {
        Integer i;
        
        DefaultTableModel model = (DefaultTableModel)frame.getjTable1().getModel();
        int clm = 2;
        for(int row=0;row<model.getRowCount()-1;row++){
            for(int col = clm; col<model.getColumnCount();++col) {
                i = random.nextInt(9);
                if(i!=0) {
                    model.setValueAt(i, row, col);
                }
            }
            clm++;
        }
    }
    
    Integer tentukan_jarak(String huruf) {
        Integer jarak;
        jarak = 0;
        
        DefaultTableModel model = (DefaultTableModel)frame.getjTable1().getModel();
        int clm = 2;
        for(int row=0;row<model.getRowCount()-1;row++){
            for(int col = clm; col<model.getColumnCount();++col) {
                int jrk = (int) model.getValueAt(row, col);
                
                if(huruf.equals("AB")||huruf.equals("BA")){
                    jarak = (Integer) model.getValueAt(0, 2);
                }else if(huruf.equals("AC")||huruf.equals("CA")){
                    jarak = (Integer) model.getValueAt(0, 3);
                }else if(huruf.equals("AD")||huruf.equals("DA")){
                    jarak = (Integer) model.getValueAt(0, 4);
                }else if(huruf.equals("AE")||huruf.equals("EA")){
                    jarak = (Integer) model.getValueAt(0, 5);
                }else if(huruf.equals("AF")||huruf.equals("FA")){
                    jarak = (Integer) model.getValueAt(0, 6);
                }else if(huruf.equals("BC")||huruf.equals("CB")){
                    jarak = (Integer) model.getValueAt(1, 3);
                }else if(huruf.equals("BD")||huruf.equals("DB")){
                    jarak = (Integer) model.getValueAt(1, 4);
                }else if(huruf.equals("BE")||huruf.equals("EB")){
                    jarak = (Integer) model.getValueAt(1, 5);
                }else if(huruf.equals("BF")||huruf.equals("FB")){
                    jarak = (Integer) model.getValueAt(1, 6);
                }else if(huruf.equals("CD")||huruf.equals("DC")){
                    jarak = (Integer) model.getValueAt(2, 4);
                }else if(huruf.equals("CE")||huruf.equals("EC")){
                    jarak = (Integer) model.getValueAt(2, 5);
                }else if(huruf.equals("CF")||huruf.equals("FC")){
                    jarak = (Integer) model.getValueAt(2, 6);
                }else if(huruf.equals("DE")||huruf.equals("ED")){
                    jarak = (Integer) model.getValueAt(3, 5);
                }else if(huruf.equals("DF")||huruf.equals("FD")){
                    jarak = (Integer) model.getValueAt(3, 6);
                }else if(huruf.equals("EF")||huruf.equals("FE")){
                    jarak = (Integer) model.getValueAt(4, 6);
                }
            }
            clm++;
        }
        
        return jarak;
    }
    
    Integer seleksi_kromosom(double[] c){
        Integer kromosom=0,rnd;
        rnd = random.nextInt(100);
        double rdm = (double) (rnd*0.01);
        
        if((rdm>=0)&&(rdm<=c[0])){
            kromosom = 0;
        }else if((rdm>=c[0]+0.0001)&&(rdm<=c[1])){
            kromosom = 1;
        }else if((rdm>=c[1]+0.0001)&&(rdm<=c[2])){
            kromosom = 2;
        }else if((rdm>=c[2]+0.0001)&&(rdm<=c[3])){
            kromosom = 3;
        }else if((rdm>=c[3]+0.0001)&&(rdm<=c[4])){
            kromosom = 4;
        }else if((rdm>=c[4]+0.0001)&&(rdm<=c[5])){
            kromosom = 5;
        }
        
        return kromosom;
    }
    
    String[] mutasi(String[] k) {
        int index=0;
        int iterasi = (int) ((k.length*5)*0.1);
        String temp;
        String[] kv = new String[5];
        
        for(int i=0; i<iterasi;i++) {
            int rand = random.nextInt((k.length*5)-1);
            
            index = tentukan_index(rand);
            
            for(int j=0;j<k[index].length();j++){
                kv[j] = k[index].substring(j,j+1);
            }
            
            int cek = 0;
            while (cek==0) {
                int rj = random.nextInt(4);
                if(rj!=(rand % 5)){
                    temp = kv[rj];
                    kv[rj] = kv[rand%5];
                    kv[rand%5] = temp;
                    cek=1;
                }
            }
            k[index] = "";
            for(int l= 0;l<kv.length;l++){
                k[index] = k[index]+kv[l];
            }
        }
        
        return k;
    }
    
    int tentukan_index(int rand){
        int index=0;
        
        if((rand>=0)&&(rand<=4)){
            index=0;
        }else if((rand>=5)&&(rand<=9)){
            index=1;
        }else if((rand>=10)&&(rand<=14)){
            index=2;
        }else if((rand>=15)&&(rand<=19)){
            index=3;
        }else if((rand>=20)&&(rand<=24)){
            index=4;
        }else if((rand>=25)&&(rand<=29)){
            index=5;
        }
        
        return index;
    }
    
}
